import {Component, OnInit, ViewChild} from '@angular/core';
import {DateUtilsService} from '../services/date-utils.service';
import {MatCardTitle} from '@angular/material';

@Component({
  selector: 'app-restaurant-detail',
  templateUrl: './restaurant-detail.component.html',
  styleUrls: ['./restaurant-detail.component.less']
})
export class RestaurantDetailComponent implements OnInit {
  // slideshow component for manipulating with slides. undefined for first opening of component
  @ViewChild('mySuperSlideShow') mySuperSlideShow: any;

  restaurantData: IRestaurantNode;
  imageSources: string[];
  openDetail = false;
  gMapUrl: string;

  ngOnInit() {
  }

  /**
   * displays the content of the compoennt and passes the data. called from grid and map component
   */
  showDetail(data: IRestaurantNode) {
    this.restaurantData = data;
    this.generateImages();
    this.generateGMapUrl();
    if (this.mySuperSlideShow) {
      // resetting index because old index can be higher then new maximum of new slides
      // eg. old slider is on slide nr.5 but new slider has just 3 slides
      this.mySuperSlideShow.slideIndex = 0;
    }

    // displaying card
    this.openDetail = true;
  }

  /**
   * generates url for google maps with current lat/long
   */
  generateGMapUrl() {
    const coordinates = DateUtilsService.parseCoordinates(this.restaurantData);
    this.gMapUrl = 'https://www.google.com/maps/search/?api=1&query=' +
      coordinates.lat + ',' + coordinates.long;
  }

  /**
   * prepares array of images for slider
   */
  generateImages() {
    // resetting
    this.imageSources = [];
    this.imageSources = this.restaurantData.media.map((item) => item.url);
    if (this.imageSources.length === 0) {
      // no images found, using default
      this.imageSources.push('assets/default.jpg');
    }
  }
}

export interface IRestaurantNode {
  trcid: string;
  location: {
    latitude: string;
    longitude: string;
    adress: string;
    city: string;
    zipcode: string;
  };
  title: string;
  details: {
    en: {
      calendarsummary: string;
      shortdescription: string;
    }
  };
  urls: string[];
  media: {
    url: string;
    main: string;
  }[];
}
