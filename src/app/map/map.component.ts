import {Component, Input, OnInit} from '@angular/core';
import {BackendApiServiceService} from '../services/backend-api-service.service';
import {DateUtilsService} from '../services/date-utils.service';
import {IRestaurantNode, RestaurantDetailComponent} from '../restaurant-detail/restaurant-detail.component';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.less']
})
export class MapComponent implements OnInit {

  // data for map
  lat: any = 0;
  lng: any = 0;
  zoom = 9;
  // will be filled in constructor
  markers: IMarker[] = [];

  private rawRestaurantsData: IRestaurantNode[] = [];

  // instance of Detail component to show detail
  @Input() restaurantDetail: RestaurantDetailComponent;

  constructor(private api: BackendApiServiceService) {
  }

  ngOnInit() {
    this.api.getRestaurants().then((data: IRestaurantNode[]) => {
      data.forEach((item) => {
        // harvesting data and storing in local property
        this.rawRestaurantsData[item.trcid] = item;
        // parse and store data for map
        const coordinates = DateUtilsService.parseCoordinates(item);
        const tmp: IMarker = {
          trcid: item.trcid,
          lat: coordinates.lat,
          lng: coordinates.long,
          title: item.title
        };
        if (item.media.length > 0) {
          tmp.iconUrl = item.media[0].url;
        }

        this.markers.push(tmp);
      });
      this.assignLatAndLng();
    });
  }

  /**
   * computes the average from the lat/long for selecting the center of map
   */
  assignLatAndLng() {
    const lats = this.markers.map(i => parseFloat(i.lat));
    const lngs = this.markers.map(i => parseFloat(i.lng));
    this.lat = lats.reduce((a, b) => a + b) / this.markers.length;
    this.lng = lngs.reduce((a, b) => a + b) / this.markers.length;
  }

  /**
   * opens the detail card
   */
  onMarkerClicked(item) {
    this.restaurantDetail.showDetail(this.rawRestaurantsData[item.trcid]);
  }
}

// just an interface for type safety.
interface IMarker {
  trcid: string;
  lat: string;
  lng: string;
  title: string;
  iconUrl?: string;
}
