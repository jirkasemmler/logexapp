import {Component, Input, OnInit} from '@angular/core';
import {BackendApiServiceService} from '../services/backend-api-service.service';
import {CustomSetFilterComponent} from '../custom-set-filter/custom-set-filter.component';
import {DateUtilsService} from '../services/date-utils.service';
import {IRestaurantNode, RestaurantDetailComponent} from '../restaurant-detail/restaurant-detail.component';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.less']
})
export class GridComponent implements OnInit {

  frameworkComponents;
  rowData: IRestaurantNode[];

  @Input() restaurantDetail: RestaurantDetailComponent;
  // configuration of columns for ag-grid
  columnDefs = [
    {
      headerName: 'Name',
      field: 'title',
      filter: 'agTextColumnFilter',
      filterParams: {suppressAndOrCondition: true}
    },
    {
      headerName: 'City', field: 'location.city', filter: 'setFilter',
      filterParams: {suppressAndOrCondition: true}
    },
    {
      headerName: 'Zip', field: 'location.zipcode',
      filterParams: {suppressAndOrCondition: true}
    },
    {
      headerName: 'Address', field: 'location.adress',
      filterParams: {suppressAndOrCondition: true}
    },
    {
      // date column -> have to define own formatter and comparator
      headerName: 'Start', field: 'dates.startdate', filter: 'agDateColumnFilter',
      valueFormatter: (data) => {
        // rendering format. m-d-YYYY -> dd.mm.YYYY
        return (data.value ? DateUtilsService.parseDate(data.value).toLocaleDateString() : '');
      },
      filterParams: {
        suppressAndOrCondition: true,
        // comparator for filtering
        comparator: DateUtilsService.dateFilterComparator
      },
      // comparator for sorting
      comparator: DateUtilsService.dateSortingComparator
    }
  ];


  constructor(private api: BackendApiServiceService) {
    // building the setFilter
    this.frameworkComponents = {setFilter: CustomSetFilterComponent};
  }

  ngOnInit() {
    // load async data. data are passed to grid in component def in tpl
    this.rowData = this.api.getRestaurants();
  }

  onGridReady(params) {
    params.api.sizeColumnsToFit();
  }

  /**
   * displays the detail
   */
  onCellClicked(node) {
    this.restaurantDetail.showDetail(node.data);
  }
}


