import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class BackendApiServiceService {

  constructor(private http: HttpClient) {
  }

  data = null;
  apiUrlPrefix = 'assets/data';

  /**
   * builds the url for http requests
   * @param endPoint - name of the rest resource
   * @returns full URL
   */
  private buildResourceUrl(endPoint: string) {
    return [this.apiUrlPrefix, endPoint].join('/');
  }

  /**
   * returns promise for restaurants
   */
  public getRestaurants() {
    if (this.data === null) {
      this.data = this.http.get(this.buildResourceUrl('establishment.json')).toPromise();
    }
    return this.data;

  }
}
