import {async, TestBed} from '@angular/core/testing';

import { BackendApiServiceService } from './backend-api-service.service';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {HttpClientModule} from "@angular/common/http";

describe('BackendApiServiceService', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [BackendApiServiceService],
      imports: [HttpClientModule, HttpClientTestingModule]
    })
      .compileComponents();
  }));
  it('should be created', () => {
    const service: BackendApiServiceService = TestBed.get(BackendApiServiceService);
    expect(service).toBeTruthy();
  });
});
