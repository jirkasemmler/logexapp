import {Injectable} from '@angular/core';
import {GridComponent} from '../grid/grid.component';

@Injectable({
  providedIn: 'root'
})
export class DateUtilsService {

  /**
   * compares dates for ag-grid comparator. returns positive value if first date is bigger then the second one
   */
  static dateSortingComparator(date1: string, date2: string) {
    const date1Number = DateUtilsService.monthToComparableNumber(date1);
    const date2Number = DateUtilsService.monthToComparableNumber(date2);
    if (date1Number === null && date2Number === null) {
      return 0;
    }
    if (date1Number === null) {
      return -1;
    }
    if (date2Number === null) {
      return 1;
    }
    return date1Number - date2Number;
  }

  /**
   * comparator for ag-grid filter
   */
  static dateFilterComparator(filterLocalDateAtMidnight: Date, cellValue: string) {
    const cellDate = DateUtilsService.parseDate(cellValue);
    if (cellDate < filterLocalDateAtMidnight) {
      return -1;
    } else {
      return (cellDate > filterLocalDateAtMidnight) ? 1 : 0;
    }
  }

  /**
   * returns unix time for passed date string
   */
  static monthToComparableNumber(date: string) {
    if (date === undefined || date === null || date.length !== 10) {
      return null;
    }
    const dateObj = DateUtilsService.parseDate(date);
    return dateObj.getTime();
  }

  /**
   * returns date object for passed string in format DD-MM-YYY
   */
  static parseDate(dateString: string) {
    const split = dateString.split('-');
    return new Date(parseInt(split[2]), (parseInt(split[1]) - 1), parseInt(split[0]));
  }

  /**
   * parses the coordinates from coma separated decimal to point separated
   */
  static parseCoordinates(node) {
    return {'lat': node.location.latitude.replace(',', '.'), 'long': node.location.longitude.replace(',', '.')};
  }
}
