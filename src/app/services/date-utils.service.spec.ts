import {TestBed} from '@angular/core/testing';

import {DateUtilsService} from './date-utils.service';

describe('DateUtilsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DateUtilsService = TestBed.get(DateUtilsService);
    expect(service).toBeTruthy();
  });

  it('should parse date', () => {
    const date = DateUtilsService.parseDate('07-06-1993');
    expect(date instanceof Date).toBeTruthy();
    expect(date.getDate()).toEqual(7);
    expect(date.getFullYear()).toEqual(1993);
  });

  it('should get unix time', () => {
    const date = DateUtilsService.monthToComparableNumber('07-06-1993');
    // extra 000 because of milliseconds from getTime()
    expect(date).toEqual(739404000000);
  });
  it('should try to get unix time but receive null', () => {
    const date = DateUtilsService.monthToComparableNumber(undefined);
    expect(date).toEqual(null);
  });

  it('should parse coordinates', () => {
    const node = {
      location: {latitude: '50,90', longitude: '10,20'}
    };
    expect(DateUtilsService.parseCoordinates(node)).toEqual({lat: '50.90', long: '10.20'});
  });

  it('filter dates', () => {
    expect(DateUtilsService.dateFilterComparator(new Date(1993, 7, 6), '17-02-2011')).toEqual(1);
    expect(DateUtilsService.dateFilterComparator(new Date(1993, 7, 6), '17-02-1980')).toEqual(-1);
    expect(DateUtilsService.dateFilterComparator(new Date(1993, 7, 6), '06-08-1993')).toEqual(0);
  });

  it('sort dates', () => {
    expect(DateUtilsService.dateSortingComparator('', '')).toEqual(0);
    expect(DateUtilsService.dateSortingComparator('17-02-1980', '')).toEqual(1);
    expect(DateUtilsService.dateSortingComparator('', '17-02-1980')).toEqual(-1);
    expect(DateUtilsService.dateSortingComparator('07-02-1980', '17-02-1980')).toBeLessThan(0);
    expect(DateUtilsService.dateSortingComparator('27-02-1980', '17-02-1980')).toBeGreaterThan(0);
  });

});
