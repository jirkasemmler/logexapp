import {BrowserModule} from '@angular/platform-browser';
import { NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {SlideshowModule} from 'ng-simple-slideshow';
import {AgmCoreModule} from '@agm/core';
import {FormsModule} from '@angular/forms';
import {AgGridModule} from 'ag-grid-angular';

import {GridComponent} from './grid/grid.component';
import {CustomSetFilterComponent} from './custom-set-filter/custom-set-filter.component';
import {RestaurantDetailComponent} from './restaurant-detail/restaurant-detail.component';
import {MapComponent} from './map/map.component';
import {AppComponent} from './app.component';

import {
  MatButtonModule,
  MatCardModule,
  MatListModule,
  MatGridListModule,
  MatTabsModule,
  MatIconModule, MatToolbarModule,
} from '@angular/material';

const materialModules = [
  MatButtonModule,
  MatCardModule,
  MatListModule,
  MatGridListModule,
  MatTabsModule,
  MatIconModule,
  MatToolbarModule
];

@NgModule({
  declarations: [
    AppComponent,
    GridComponent,
    CustomSetFilterComponent,
    RestaurantDetailComponent,
    MapComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    SlideshowModule,
    AgGridModule.withComponents([CustomSetFilterComponent]),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDmFaC9RulCzpFlGep3WnccmfCJ6wBBULg'
    }),
    ...materialModules
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
