import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {IFilterAngularComp} from 'ag-grid-angular';
import {IAfterGuiAttachedParams, IDoesFilterPassParams, IFilterParams, RowNode} from 'ag-grid-community';

@Component({
  selector: 'app-custom-set-filter',
  templateUrl: './custom-set-filter.component.html',
  styleUrls: ['./custom-set-filter.component.less']
})


export class CustomSetFilterComponent implements IFilterAngularComp, OnInit {

  constructor() {
  }

  private params: IFilterParams;
  private valueGetter: (rowNode: RowNode) => any;
  public selectedOptions = [];

  public items = [];

  @ViewChild('input', {read: ViewContainerRef}) public input;

  private data: any;

  ngOnInit() {
  }

  agInit(params: any): void {
    this.params = params;
    this.valueGetter = params.valueGetter;
    const values = [];
    this.items = [];
    this.data = params.api.rowModel.rowsToDisplay;

    this.data.forEach((node) => values.push(node.data.location.city));

    // making items list unique
    this.items = values.filter((value, index, self) => self.indexOf(value) === index);

    this.items.sort();
  }

  isFilterActive(): boolean {
    return this.selectedOptions.length !== 0;
  }

  /**
   * real filtering on data model based on selected items in set filter
   */
  doesFilterPass(params: IDoesFilterPassParams): boolean {
    return this.selectedOptions.indexOf(this.valueGetter(params.node).toString()) >= 0;
  }

  getModel(): any {
    return {value: this.selectedOptions};
  }

  setModel(model: any): void {
    this.selectedOptions = model ? model.value : [];
  }

  ngAfterViewInit(params: IAfterGuiAttachedParams): void {
    setTimeout(() => {
      this.input.element.nativeElement.focus();
    });
  }

  onChange(newValue): void {
    this.params.filterChangedCallback();
  }
}

