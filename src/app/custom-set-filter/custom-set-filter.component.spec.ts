import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomSetFilterComponent } from './custom-set-filter.component';
import {CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA} from "@angular/core";

describe('CustomSetFilterComponent', () => {
  let component: CustomSetFilterComponent;
  let fixture: ComponentFixture<CustomSetFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CustomSetFilterComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));


  beforeEach(() => {
    fixture = TestBed.createComponent(CustomSetFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
