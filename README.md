# Logexapp

Project description: test assignment for Logex company. First app in Angular6+.

Contains 3 components to render grid, map and detail. Using ag-grid for grid (with custom filters and comparators), agm for maps and material design for fancy look.

Spent 25.66h in total (https://prnt.sc/lbkxv6). Mostly busy with new environment of Angular6 (previous exp with AngularJS only) and fighting with ag-grid.

Working demo : http://logex.jirisemmler.eu/

Repository : https://bitbucket.org/jirkasemmler/logexapp/src/master/

Assignment: http://logex.jirisemmler.eu/logex-test.pdf (implemented Bonus1)
## Commands
Generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.1.

run `ng serve` for running the app locally, 

run `ng build` for building dist version. The code will be present in dist/ then.

run `ng test` for running tests. Note: two tests are failing. Problem with dependency
